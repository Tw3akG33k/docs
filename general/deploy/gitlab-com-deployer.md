## Overview

GitLab.com deployments are initiated by GitLab Chatops, this triggers
a new pipeline on the [Deployer](https://ops.gitlab.net/gitlab-com/gl-infra/deployer) project.

To see all available options for deploy run `/chatops run deploy --help`:

```
Usage: deploy [VERSION] [OPTIONS]
-h, --help                Shows this help message
  --production              Deploy to production, instead of staging
  --canary                  Only deploy to a canary, instead of the entire environment
  --warmup                  Only perform a warmup, instead of a full deploy
  --allow-precheck-failure  Allow the version and alert prechecks to fail
  --skip-haproxy            Skip the ansible haproxy tasks
  --rollback                Initiate a rollback deploy, this will order the deploy steps differently for rollback
```

## Creating a new deployment for _upgrading_ GitLab

Deployments are initiated using GitLab Chatops.
For example, to initiate a deployment of 11.8.0 to **staging**:

```
/chatops run deploy 11.8.0.ee.0
```

## Creating a new deployment for _rolling back_ GitLab

**IMPORTANT: Before initiating a rollback ensure that the steps in the
[SRE runbook for rollback](https://gitlab.com/gitlab-com/runbooks/blob/master/howto/upgrade-and-rollback.md)
are followed and the oncall engineer is engaged in the process**

Application rollbacks are initiated using GitLab Chatops. For example, to initiate a rollback to 11.7.0 on **staging**:
```
/chatops run deploy 11.7.0.ee.0 --rollback
```


When a chatops command is received, the chatops project will initiate a [deployer pipeline](https://ops.gitlab.net/gitlab-com/gl-infra/deployer/pipelines)
job for the version and environment specified in the command. It is also
possible to manually trigger a pipeline using the following CI variables:

* `DEPLOY_ENVIRONMENT`: **required**
* `DEPLOY_VERSION`: **required**
* `DEPLOY_ROLLBACK`: **optional**, set to `yes` to initiate a rollback pipeline

It's also possible to start a deployer pipeline by initiating a new pipeline
on the [deployer project](https://ops.gitlab.net/gitlab-com/gl-infra/deployer)
**master branch** and setting the environment and version CI variables.

_Note that all pipelines initiated on branches will automatically set the
`--dry-run` flag which can be useful for testing._

![Deployer Pipeline](../images/deployer-example.png)


### CICD Pipeline for a Deployer Upgrade

For a single environment (production, staging, etc.), the following stages are run:

### Upgrade (default)

```mermaid
graph LR;
    a>prepare] ==> b>migrations];
    b ==> c>gitaly deploy];
    subgraph fleet;
    c -.- d>sidekiq];
    c -.- d1>git];
    c -.- d2>web];
    c -.- d3>api];
    c -.- d4>pages];
    c -.- d5>registry];
    c -.- d6>mailroom];
    end
    d ==> e>postdeploy migrations];
    e ==> f>cleanup];
    f ==> g>gitlab-qa];
```

### CICD Pipeline for a Deployer Rollback

A rollback pipeline has the same stages, except that the `gitaly`
stage is placed after the `fleet` stage. This is necessary because
it's possible that rails will have changes that are incompatible with
earlier versions of Gitaly.

```mermaid
graph LR;
    a>prepare] ==> c>migrations];
    subgraph fleet;
    c -.- d>sidekiq];
    c -.- d1>git];
    c -.- d2>web];
    c -.- d3>api];
    c -.- d4>pages];
    c -.- d5>registry];
    c -.- d6>mailroom];
    end
    d ==> b>gitaly deploy];
    b ==> e>postdeploy migrations];
    e ==> f>cleanup];
    f ==> g>gitlab-qa];
style b fill:#E0FFFF;
```

#### Rollback considerations for database migrations

* Before initiating a rollback, background migrations should be evaluated. The
  DBRE oncall should asses the impact of rolling back. Note that clearing
  the background migration queue may not be the best course of action as these
  migrations should be backwards compatible with previous application versions.
  For information on how to clear queues see
  [the sidekiq troubleshooting guide](https://gitlab.com/gitlab-com/runbooks/blob/master/troubleshooting/large-sidekiq-queue.md#viewing-and-killing-jobs-from-the-queue)
* If the current version introduced one or more post-deployment migrations,
  these migrations must be reverted before rolling back the code changes.
  This is a manual process and should be assessed by
  a DBRE before the rollback is initiated.
  https://gitlab.com/gitlab-org/release/framework/issues/234 discusses
  how to we can better deal with post-deploy migrations in the context of
  rollbacks.
* Regular migrations _can be reverted_ but **they are not** reverted in
  the rollback pipeline. Migrations are designed to be backwards compatible
  with the previous version of application code.
* Rolling back more than one version without a very thorough review should never
  be attempted.


## Upgrade / Rollback Deploy STages

* **prepare**: The prepare stage is a single job that is responsible for all
  non-destructive changes before a deployment. This is the general place we put
  checks and notifications before continuing with a deployment. The following
  tasks are executed in the prepare stage:
  * slack notifications
  * notes the start time of the deploy
  * checks to see if the package is indexed in packagecloud
  * verifies haproxy status
  * verifies that there are no critical alerts
  * verifies the version is consistent across the fleet

* **migrations**: This stage runs online database migrations for staging and canary deployments.
  We do not run online migrations for production deployments because they are handled
  by the canary stage

* **gitaly deploy**: The Gitaly deploy happens before the rest of the fleet in case there are rails
  changes in the version being deployed that take advantage of new Gitaly features. If the Gitaly version
  is not changed, the omnibus package update is skipped, and updated later
  when the chef is run after the pipeline completes.  If the Gitaly version
  is updated, a Gitaly restart is issued which will often result in errors on
  GitLab.com.

* **fleet deploys**: The rest of the fleet is deployed to in parallel, the logic
  is identical for each set of roles. The omnibus is upgraded, gitlab-ctl reconfigure is
  run and if the service version changes a restart is issued. In the case of rails
  we always issue a HUP instead of a restart.

* **postdeploy migrations**: [Post-deploy migrations](https://docs.gitlab.com/ee/development/post_deployment_migrations.html#post-deployment-migrations)
  are run last and may be a
  point-of-no-return for upgrades as it might make a change that is not
  backwards compatible with previous versions of the application.

* **cleanup**: Like _prepare_, the cleanup stage is a single job that handles
  any post-install tasks that need to run at the end of deployment. This
  includes slack notifications, grafana annotations and starting Chef across the
  fleet.

* **gitlab-qa**: The very last stage of the pipeline runs a set of QA smoke tests
  against an environment on which the deploy is running.

### Omnibus Installation

The omnibus installation is handled by a [task file](https://gitlab.com/gitlab-com/gl-infra/deploy-tooling/blob/master/common_tasks/install_gitlab_ee.yml)
that has the following steps:

```mermaid
graph LR;
    a>stop chef] ==> b>rollback any existing patches];
    b ==> c>install gitlab-ee];
    c ==> d>apply any new patches];
    d ==> e>run gitlab-ctl reconfigure];
    e ==> f>restart services if necessary];
```

Note that existing patches are rolled back and new patches are applied on every
deploy. Deployer does this by referencing the [patcher repository](https://ops.gitlab.net/gitlab-com/gl-infra/patcher)
as a submodule for applying post-deployment-patches during deployments.

For more information about how patches are applied to the fleet see the
[GitLab post-deployment patcher](https://gitlab.com/gitlab-org/release/docs/blob/master/general/deploy/post-deployment-patches.md)
documentation.

### Repositories overview

```mermaid
graph TD
    subgraph gl-infra/patcher;
    a1[gitlab-ci.yml];
    a2[deploy-tooling submodule];
    end;
    subgraph gl-infra/deployer;
    b1[gitlab-ci.yml];
    b2[deploy-tooling submodule];
    b3[patcher submodule];
    end;
```
* [gl-infra/deployer](https://ops.gitlab.net/gitlab-com/gl-infra/deployer/) is the repository where the CICD configuration is maintained
  for defining the pipeline. It is sourced on https://ops.gitlab.net with a public
  mirror on https://gitlab.com.
* [gl-infra/patcher](https://ops.gitlab.net/gitlab-com/gl-infra/patcher/) is the repository where post-deployment patches live, it is a
  private repo that is sourced on https://ops.gitlab.net
* [gl-infra/deploy-tooling](https://ops.gitlab.net/gitlab-com/gl-infra/deploy-tooling/) is a common repository that is used as a submodule
  for all other repos that require Ansible code. This repository contains the
  plays, plugins and scripts for deployment. It is sourced on
  https://ops.gitlab.net with a public mirror on https://gitlab.com.

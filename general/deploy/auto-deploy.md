## Overview

GitLab auto-deploy deployments allow us to increase the frequency
of deployments to GitLab.com from the master branch of
[gitlab-ee](https://gitlab.com/gitlab-org/gitlab-ee)
and [omnibus-gitlab](https://gitlab.com/gitlab-org/omnibus-gitlab).

### Auto-deploy branches

Approximately once a week, up until the release date on the 22nd, auto-deploy
branches are created with the following format:

```
<MAJOR>-<MINOR>-auto-deploy-<ID>
```

example: `11-10-auto-deploy-0000001`


* `MAJOR.MINOR`: This comes from the currently active milestone on the gitlab-ce
  project. It generally tracks to the release month and has the following
  requirements:
  * It must be an _active_ milestone
  * The start date must be in the _past_
  * The end date must be in the _future_
* `ID`: The pipeline `$CI_PIPELINE_IID` of the release-tools job that
  created the branch. This ID is guaranteed to increment on every auto-deploy
  branch.

Auto-deploy branches are created for the following projects:

* gitlab-ee
* gitlab-ce
* omnibus-gitlab


As soon as these branches are created, the latest green (passing) commit on the branch
will be tagged for an auto-deployment.

When omnibus-gitlab is tagged, it will result in a new EE omnibus-gitlab package and a
deployment to a pre-production environment with a deployer pipeline extending to
production.

Auto-deploy branches are _protected branches_, meaning that they require special
permission for merging and pushing and also will be mirrored on dev.gitlab.org.

### Auto-deploy tagging

For every auto-deploy deployment, there is a git tag that matches the version of
what is deployed. The auto-deploy tag has the following format:

```
<MAJOR>.<MINOR>.<ID>+<gitlab-ee sha>.<omnibus-gitlab sha>
```


* `MAJOR.MINOR`: This is the currently active milestone in the `gitlab-org`
  group on gitlab.com and follows the same requirements
  for the auto-deploy branch (see above).
* `ID`: The pipeline `$CI_PIPELINE_IID` of the release-tools job that
  created the tag. This ID is guaranteed to increment on every tag job.
* `gitlab-ee sha`: The sha of gitlab-ee for auto-deploy, it corresponds to a
  green (passing) commit on the gitlab-ee auto-deploy branch.
* `omnibus-gitlab sha`: The sha of omnibus-gitlab that will be used for the next
  auto-deploy, it corresponds to a green (passing) ref on the omnibus-gitlab
  auto-deploy branch


When the repositories are tagged, an omnibus-gitlab pipeline on dev.gitlab.org
builds the package. This pipeline will trigger a deployment
of the auto-deploy package to a pre-production environment.

### Auto-deploy schedule
<table>
<thead>
<tr>
<th>Day</th>
<th>Description</th>
</tr>
</thead>

<tr>
  <td>23rd</td>
  <td>

  * A new milestone is manually created named **11.12** with a start date of the 23rd and a due date of the 22nd
  * A CI job creates a branch named **11-12-auto-deploy-00001** in gitlab-ce, omnibus-gitlab and
    gitlab-ee from **master**
    * The merge train ensures that ce is merged into ee
    * The latest green commit of gitlab-ee is used to update versions of GitLab
      services in the omnibus-gitlab repository.
    * The commits in gitlab-ee and omnibus-gitlab are tagged with **11.12.100+aaaa.ffff**
    * The resulting auto-deploy package is deployed to a pre-production environment with a deployer
      pipeline extending to production

  </td>
</tr>
<tr>
  <td>23rd..1st</td>
  <td>

  * MRs that are merged to master are targeted for auto-deploy by manually
    adding a **Pick into 11.12** label
  * A CI job picks these MRs for auto-deploy
    * Labeled MRs are picked automatically into the **11-12-auto-deploy-00001** branch
    * The merge train ensures that ce is merged into ee
    * The next green commit on the auto-deploy branch is tagged with **11.12.101+bbbb.ffff**
    * The resulting auto-deploy package is deployed to a pre-production environment with a deployer
      pipeline extending to production
    * MRs are updated to let the MR author know that the change has been deployed
  * This process is repeated on a frequent interval, with multiple deployments
    to pre-production environments and production
  * Other MRs that are merged to master not be included until the next week's
    auto-deploy branch, **11-12-auto-deploy-00002**

  </td>
</tr>
<tr>
  <td>1st</td>
  <td>

  * Branch **11-12-auto-deploy-00002** created in gitlab-ce, omnibus-gitlab and
    gitlab-ee from **master**
  * The same automated process happens for the new auto-deploy brach, the merge
    train ensures the ce->ee merge, a green commit found, tagged and deployed
    to a pre-proudction environment with a deployer pipeline extending to
    production
  * The same picking and deploying process is now done for the new auto-deploy
    branch, **11-12-auto-deploy-00002**
  </td>
</tr>
<tr>
  <td>8th</td>
  <td>

  * Branch **11-12-auto-deploy-00003** created in gitlab-ce, omnibus-gitlab and
    gitlab-ee from **master**
  * The same picking and deploying process is now done for the new auto-deploy
    branch, **11-12-auto-deploy-00003**

  </td>
</tr>
<tr>
  <td>15th</td>
  <td>

  * Branch **11-12-auto-deploy-00004** created in gitlab-ce, omnibus-gitlab and
    gitlab-ee from **master**
  * The same picking and deploying process is now done for the new auto-deploy
    branch, **11-12-auto-deploy-00004**

  </td>
</tr>
<tr>
  <td>22nd RELEASE DAY</td>
  <td>

  * 11.12 is released
  * The commit that is currently on production will be used for the official
    release
  * Changes that are released on production will be part of the release blog
    post
  * Not all picked changes will be deployed and included in the final release
    as it is possible that not all MRs that are picked will have been deployed
    to production in time to make the release

  </td>
</tr>

<tr>
  <td>23rd</td>
  <td>

  * A new milestone is manually created named **11.13** with a start date of the 23rd and a due date of the 22nd
  * The process auto-deploy cycle repeats

  </td>
</tr>

</table>

## FAQ

### How does this change the existing code freeze on the 7th?
### How often do we auto-deploy?
### What about registry, workhorse, gitaly, pages and other components?
### How do I mark an MR so that will be picked into the auto-deploy branch?
### Does auto-deploy change how we release software to the self-managed community?
### How are auto-deploy branches different than the stable branch?
### Why are we not deploying directly from the master branch?
### How do I query what ref is running on gitlab.com or a pre-production environment?
### How will marketing know what features to put in the release blog post?

